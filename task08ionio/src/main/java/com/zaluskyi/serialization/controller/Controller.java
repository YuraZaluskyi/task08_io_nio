package com.zaluskyi.serialization.controller;

import com.zaluskyi.serialization.model.PersonSerialization;

public class Controller {
    private PersonSerialization personSerialization = new PersonSerialization();

    public void serialization(){
        personSerialization.serializePerson();
    }

    public  void deserialization(){
        personSerialization.deserializePerson();
    }

}

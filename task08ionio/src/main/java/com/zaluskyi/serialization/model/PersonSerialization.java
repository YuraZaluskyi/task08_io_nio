package com.zaluskyi.serialization.model;

import com.zaluskyi.serialization.model.Person;

import java.io.*;

public class PersonSerialization {
    private Person person = new Person("Donald", "Trump");
    private String filePerson = "personSerializable";

    public void serializePerson() {
        try (FileOutputStream foi = new FileOutputStream(filePerson);
             ObjectOutputStream oos = new ObjectOutputStream(foi)) {
            oos.writeObject(person);
            System.out.println("Person is serialized");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deserializePerson() {
        try (FileInputStream fio = new FileInputStream(filePerson);
             ObjectInputStream ois = new ObjectInputStream(fio)) {
            Person person = (Person) ois.readObject();
            System.out.println("Person is deserialized");
            System.out.println("Person - " + person);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

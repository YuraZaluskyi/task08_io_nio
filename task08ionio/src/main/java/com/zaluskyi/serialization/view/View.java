package com.zaluskyi.serialization.view;

import com.zaluskyi.serialization.controller.Controller;

import java.util.Scanner;

public class View {
    private Controller controller = new Controller();
    private Scanner sc = new Scanner(System.in);

    public void start() {
        String choice;
        while (true) {
            menu();
            choice = sc.next();
            switch (choice) {
                case "1":
                    controller.serialization();
                    break;
                case "2":
                    controller.deserialization();
                    break;
                default:
                    return;
            }
        }
    }

    private void menu() {
        System.out.println("    M E N U");
        System.out.println("1 - serialization object\n2 - deserialization object\nEXIT - press any key");
    }
}

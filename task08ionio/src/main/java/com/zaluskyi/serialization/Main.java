package com.zaluskyi.serialization;

import com.zaluskyi.serialization.view.View;

public class Main {
    public static void main(String[] args) {
        new View().start();
    }
}

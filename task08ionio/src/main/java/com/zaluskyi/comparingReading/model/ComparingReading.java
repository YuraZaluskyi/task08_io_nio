package com.zaluskyi.comparingReading.model;

import java.io.*;

public class ComparingReading {
    String pathFile = "text.txt";

    public long getTimeUsualReading() throws IOException {
        long start = System.nanoTime();
        int quantity = 0;
        InputStream inputStream = new FileInputStream(pathFile);
        inputStream.close();
        int data = inputStream.read();
        while (data != -1) {
            data = inputStream.read();
            quantity++;
        }
        long finish = System.nanoTime();
        System.out.println("quantity - " + quantity);
        return (finish - start) / 1000_000_000;
    }

    public long getTimeBufferedReading() throws IOException {
        int quantity = 0;
        long start = System.nanoTime();
        DataInputStream dataInputStream = new DataInputStream(
                new BufferedInputStream(new FileInputStream(pathFile)));
        try {
            while (true) {
                byte b = dataInputStream.readByte();
                quantity++;
            }
        } catch (EOFException e) {
        }
        dataInputStream.close();
        System.out.println("quantity - " + quantity);
        long finish = System.nanoTime();
        return (finish - start) / 1000_000_000;
    }

    public long getTimeEWithOneMbBuffer() throws IOException {
        long start = System.nanoTime();
        int sizeBuffer = 1 * 1024;
        int quantyti = 0;
        DataInputStream dataInputStream = new DataInputStream(
                new BufferedInputStream(new FileInputStream(pathFile), sizeBuffer));

        try {
            while (true) {
                byte b = dataInputStream.readByte();
                quantyti++;
            }
        } catch (EOFException e) {

        }
        return 0;
    }
}
